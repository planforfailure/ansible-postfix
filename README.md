## Description
Installs Postfix on Debian/Ubuntu derivatives with the folowing components.

- https://docs.ansible.com/ansible-core/devel/playbook_guide/playbooks_templating.html
- https://docs.ansible.com/ansible/latest/user_guide/vault.html
- https://docs.ansible.com/ansible-core/devel/playbook_guide/playbooks_variables.html


## Setup
Replace your own variables in `defaults/main.yml`. All other sensitive variables are encrypted via `ansible-vault encrypt passwd.yml`

## Role Variables
```yaml
---
loopback_network: "127.0.0.0/8"
```

## Dependencies
Ensure that if you are using this as a SMTP relay server, ensure all ports are forwarded and  add firewall rules.

## Installation

```bash
$ ansible-playbook --extra-vars '@passwd.yml' deploy.yml -K
```

```yaml
---
- hosts: all
  become: yes
  gather_facts: yes
  roles:
    - roles/ansible-postfix
```
## Licence

MIT/BSD
